var libs = {
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    menu: require('/lib/enonic/menu'),
    util: require('/lib/enonic/util/util')
};

exports.get = handleGet;


function handleGet(req) {
    var site = libs.portal.getSite();
    var content = libs.portal.getContent();
    var view = resolve('react.html');

    // Find the current component from request
    var component = libs.portal.getComponent();

    var model = {
        heading: 'React meetup'
    };

    var reactJs = libs.portal.assetUrl({
        path: 'libs/react/react.js'
    });

    var reactJsDom = libs.portal.assetUrl({
        path: 'libs/react/react-dom.js'
    });

    var reactPart = libs.portal.assetUrl({
        path: 'parts/react/reactPart.js'
    });


    return {
        body: libs.thymeleaf.render(view, model),
        pageContributions: {
            headEnd:
                [
                    "<script src='"+reactJs+"' type='text/javascript'></script>",
                    "<script src='"+reactJsDom+"' type='text/javascript'></script>",
                    "<script src='"+reactPart+"' type='text/javascript'></script>"
                ]
        }
    };
}