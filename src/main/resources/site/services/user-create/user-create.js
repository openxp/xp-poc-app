
function required(params, name) {
    var value = params[name];
    if (value === undefined) {
        throw "Parameter '" + name + "' is required";
    }

    return value;
}

function handlePost(req){
    var params = req.params;
    var bean = __.newBean("com.enonic.xp.poc.user.CreateUserService");

    bean.username = required(params, 'username');
    bean.createUser();

    return {
        contentType: 'application/json',
        body: "success"
    };
}



exports.post = handlePost;
exports.get = handlePost;