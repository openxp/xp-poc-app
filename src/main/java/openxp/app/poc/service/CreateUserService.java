package openxp.app.poc.service;

import com.enonic.xp.script.bean.BeanContext;
import com.enonic.xp.script.bean.ScriptBean;
import com.enonic.xp.security.CreateUserParams;
import com.enonic.xp.security.PrincipalKey;
import com.enonic.xp.security.SecurityService;
import com.enonic.xp.security.User;

import java.util.function.Supplier;

public class CreateUserService implements ScriptBean{

    Supplier<SecurityService> securityService;
    String username;

    public void setUsername( final String username )
    {
        this.username = username;
    }

    public void createUser() throws Exception {
        CreateUserParams createUserParams = CreateUserParams.create()
                .displayName(username)
                .email(username + "@localhost.no")
                .password("password")
                .login(username)
                .userKey(PrincipalKey.from("user:system:"+username))
                .build();
        User user = securityService.get().createUser(createUserParams);;
    }

    @Override
    public void initialize( final BeanContext context )
    {
        this.securityService = context.getService( SecurityService.class );
    }
}
