## Proof Of Concept Project.

This project contains small POC code snippets that test out functionality in Enonic XP.

## Versioning

The versioning follows the Enonic XP versioning.

## Snippets 

### CreateUserService
    
    The CreateUserService consists of:

    * Java class com.enonic.xp.poc.user.CreateUserService
    * Javascript service site/services/user-create/user-create.js

    How to test it:

    1. Deploy com.enonic.xp.poc app with ./gradlew deploy
    2. Use following url to create a user: 
       http://localhost:8080/portal/master/_/service/com.enonic.xp.poc/user-create?username=pocuser42
    3. A user should have been created in the users app under /system/users/pocusers42


###OSX Keychain Access Plugin

Example use of [OSX Keychain Access Plugin](https://bitbucket.org/openxp/osxkeychainaccesss) which fetches 
an application password from the OSX Keychain Access app, and gets those passwords out of the git repository.
See usage example in the build.gradle file. For documentation, see the projects own Readme.

###Using ES6 in parts with node / babeljs
https://plugins.gradle.org/plugin/com.moowork.node
node{
    version '4.2.1'
    download true
}
task babelJs(type:NodeTask, dependsOn:'npmInstall'){

Use .es6 suffix in javascript

###React






## License

This software is licensed under AGPL 3.0 license. See full license terms [here](http://www.enonic.com/license). Also the distribution includes
3rd party software components. The vast majority of these libraries are licensed under Apache 2.0. For a complete list please 
read [NOTICE.txt](https://github.com/enonic/xp/raw/master/NOTICE.txt).

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
